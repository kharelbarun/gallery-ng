import { Environment } from "./environment.declaration";

export const environment: Environment = {
	production: false,
	apiBaseUrl: 'http://localhost/link/to/images/api',
};

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ItemComponent } from './gallery/item/item.component';
import { PreviewComponent } from './gallery/preview/preview.component';

@NgModule({
	declarations: [
		AppComponent,
		GalleryComponent,
		ItemComponent,
		PreviewComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }

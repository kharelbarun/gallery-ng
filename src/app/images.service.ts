/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Image } from './image';
import { environment } from '../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ImagesService {

	constructor(
		private http: HttpClient
	) {
	}

	getUrl(): string {
		return environment.apiBaseUrl+'/image/';
	}

	getImages(): Observable<Array<Image>> {
		return this.http.get<any[]>(this.getUrl())
			.pipe(
				map(images => images.map(image => ({
					url: image.url,
					caption: image.caption,
					altText: image.alt_text,
					titleText: image.title_text,
				})))
			)
		;
	}
}

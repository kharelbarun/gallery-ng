/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Image } from 'src/app/image';

@Component({
	selector: 'app-gallery-image-list-item',
	templateUrl: './item.component.html',
	styleUrls: ['./item.component.css'],
	host: {
		class: '-image-list-item',
	},
})
export class ItemComponent {

	@Input() image!: Image;
	@Output('on-open-preview') onOpenPreview = new EventEmitter<void>();

}

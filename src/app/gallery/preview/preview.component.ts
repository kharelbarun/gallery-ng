/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Image } from 'src/app/image';

@Component({
	selector: 'app-gallery-preview',
	templateUrl: './preview.component.html',
	styleUrls: ['./preview.component.css'],
	host: {
		class: '-preview',
		tabIndex: '-1',
		'(keydown)': "onKeydown($event)",
	},
})
export class PreviewComponent {

	@Input('image') image!: Image;
	@Input('has-previous') hasPrevious!: boolean;
	@Input('has-next') hasNext!: boolean;
	@Output('on-show-previous') onShowPrevious = new EventEmitter<void>();
	@Output('on-show-next') onShowNext = new EventEmitter<void>();
	@Output('on-close-preview') onClosePreview = new EventEmitter<void>();

	hintIsOpen: boolean = false;

	constructor(private previewElement: ElementRef) {
	}

	ngAfterViewInit() {
		document.body.style.overflow = 'hidden';
		this.previewElement.nativeElement.focus();
	}

	ngOnDestroy() {
		document.body.style.removeProperty('overflow');
		this.previewElement.nativeElement.blur();
	}

	onKeydown(ev: KeyboardEvent) {
		switch (ev.key) {
			case 'ArrowLeft':
				this.onShowPrevious.emit();
				break;
			case 'ArrowRight':
				this.onShowNext.emit();
				break;
			case 'Escape':
				this.onClosePreview.emit();
				break;
		}
	}

	openHint() {
		this.hintIsOpen = ! this.hintIsOpen;
	}
}

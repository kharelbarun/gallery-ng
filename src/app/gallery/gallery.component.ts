/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, Input } from '@angular/core';
import { Image } from '../image';

@Component({
	selector: 'app-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.css'],
	host: {
		style: 'display: block;',
		class: 'container gallery',
	},
})
export class GalleryComponent {

	@Input() images: Array<Image> = [];
	previewIndex: number = -1;
	previewImage: Image|null = null;
	hasPrevious: boolean = false;
	hasNext: boolean = false;

	openPreview(index: number) {
		this.previewIndex = index;
		this.previewImage = this.images[index];
		this.updatePreviousNext(index);
	}

	updatePreviousNext(previewIndex: number) {
		this.hasPrevious = previewIndex > 0;
		this.hasNext = previewIndex < this.images.length - 1;
	}

	showPrevious() {
		if (this.previewIndex > 0) {
			this.previewIndex--;
			this.previewImage = this.images[this.previewIndex];
			this.updatePreviousNext(this.previewIndex);
		}
	}

	showNext() {
		if (this.previewIndex < this.images.length - 1) {
			this.previewIndex++;
			this.previewImage = this.images[this.previewIndex];
			this.updatePreviousNext(this.previewIndex);
		}
	}

	closePreview() {
		this.previewIndex = -1;
		this.previewImage = null;
	}
}

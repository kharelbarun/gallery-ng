export interface Image {
	url: string;
	caption: string;
	altText: string;
	titleText: string;
}

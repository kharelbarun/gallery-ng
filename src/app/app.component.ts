/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component } from '@angular/core';
import { forkJoin, map, Observable, timer } from 'rxjs';
import { Image } from './image';
import { ImagesService } from './images.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	images: Array<Image> = [];
	hasImageRequestCompleted: boolean = false;
	hasImageRequestSucceed: boolean = false;

	constructor(
		private imagesService: ImagesService,
	) {
	}

	ngOnInit() {
		this.getImages().subscribe({
			next: images => {
				this.images = images;
				this.hasImageRequestCompleted = true;
				this.hasImageRequestSucceed = true;
			},
			error: () => {
				this.hasImageRequestCompleted = true;
			},
		});
	}

	getImages(): Observable<Array<Image>> {
		// takes 2 seconds at minimum for demonstration purpose
		return forkJoin([
			this.imagesService.getImages(),
			timer(2000)
		]).pipe(map(([data]) => data));
	}
}
